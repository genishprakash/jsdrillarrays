function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
    if(!Array.isArray(elements)){
        return `Works only with array`
    }
    let result=[]
    for(let index=0;index<elements.length;index++){
        result[index]=cb(elements[index],index)
    }
    return result
}
module.exports=map